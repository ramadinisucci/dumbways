<!DOCTYPE html>
<html>
<head>
	<title>ADD BUKU</title>
</head>
<body>

<form action="proses_addbuku.php" method="POST">
	<div class="form-group">
		<label>Nama Buku</label>
		<input type="text" name="name" placeholder="Nama Buku" required="required">

		<label>Category</label>
		<select name="category_id">
			<option value="">Pilih Category</option>

			<?php
			include 'koneksi.php';
			while ($data2 = mysqli_fetch_array($sql2)) {
			?>

			<option value="<?php echo $data2['category_id'] ?>"></option>


			<?php
			}
			?>
		</select>
		<br>
		<br>

		<label>Nama Penulis</label>
		<select name="writer_id">
			<option value="">Pilih Penulis</option>

			<?php
			include 'koneksi.php';
			while ($data3 = mysqli_fetch_array($sql3)) {
			?>

			<option value="<?php echo $data3['writer_id'] ?>"></option>


			<?php
			}
			?>
		</select>
		<br>
		<br>

		<label>Tahun Terbit</label>
		<input type="year" name="publication_year" placeholder="Tahun" required="required">
		<br>
		<br>

		<button type="submit">ADD</button>
	</div>
</form>

</body>
</html>